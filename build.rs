use std::env;
use std::process::Command;

fn main() {
    let src = env::current_dir().unwrap();
    let out_dir = env::var("OUT_DIR").unwrap();

    Command::new("make")
        .arg(&format!("-j{}", env::var("NUM_JOBS").unwrap()))
        .arg(&format!("BUILD_DIR={}", out_dir))
        .arg("CFLAGS=-fPIC")
        .current_dir(&src.join("libslirp"))
        .status()
        .unwrap();

    println!("cargo:rustc-link-search=native={}", out_dir);
    println!("cargo:rustc-link-lib=static=slirp");

    pkg_config::find_library("glib-2.0").unwrap();
}
